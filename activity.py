"""
	Specification
	1. Create an abstract class called Animal that has the following abstract methods
	Abstract Methods: 
	* eat(food)
	* make_sound()

	2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:

	Properties:
	* name
	* breed
	* age

	Methods:
	* getters and setters
	* implementation of abstract methods
	* call()
"""

from abc import ABC, abstractmethod
class Animal(ABC):
 
    def move(self):
        pass
 
class Dog1(Animal):
 
    def move(self):
        print("Eaten Steak")
 
class Cat1(Animal):
 
    def move(self):
        print("Bark! Woof! Arf!")
 
class Dog2(Animal):
 
    def move(self):
        print("Here Isis")
 
class Cat2(Animal):
 
    def move(self):
        print("Serve me Tuna")

class Dog3(Animal):
 
    def move(self):
        print("Miaow! Nyaw! Nyaaaa!")

class Cat3(Animal):
 
    def move(self):
        print("Puss, Come on!")
         
# Test Cases
R = Dog1()
R.move()
 
K = Cat1()
K.move()
 
R = Dog2()
R.move()
 
K = Cat2()
K.move()

R = Dog3()
R.move()

K = Cat3()
K.move()